'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

const DIM = 100;
const NUM_STEPS = 100;

function getNeighbors(x,y) {
    var neighbors = [];
    if ((x-1 >=  0) && (y-1 >=  0)) { neighbors.push( [x-1,y-1] ); }
    if ((x-0 >=  0) && (y-1 >=  0)) { neighbors.push( [x-0,y-1] ); }
    if ((x+1 < DIM) && (y-1 >=  0)) { neighbors.push( [x+1,y-1] ); }
    if ((x-1 >=  0) && (y-0 >=  0)) { neighbors.push( [x-1,y-0] ); }
    if ((x+1 < DIM) && (y-0 >=  0)) { neighbors.push( [x+1,y-0] ); }
    if ((x-1 >=  0) && (y+1 < DIM)) { neighbors.push( [x-1,y+1] ); }
    if ((x-0 >=  0) && (y+1 < DIM)) { neighbors.push( [x-0,y+1] ); }
    if ((x+1 < DIM) && (y+1 < DIM)) { neighbors.push( [x+1,y+1] ); }
    return neighbors;
    }

function drawMap(map) {
    for (var y = 0; y < DIM; y++) {
        for (var x = 0; x < DIM; x++) {
            if (map[x][y]) { process.stdout.write("#"); }
            else { process.stdout.write("."); }
        }
        console.log();
    }
}

function part1(filename) {

    // Init screen
    var pixels = new Array();
    for (var i = 0; i < DIM; i++) {
        pixels[i] = new Array(DIM).fill(false);
    }

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    // Initialize screen state
    input.map((row,y) => {
        var chars = row.split("");
        chars.map((char,x) => {
            switch (char) {
                case "#":
                    pixels[x][y] = true;
                    break;
                case ".":
                    pixels[x][y] = false;
                    break;
                default:
                    console.log("ERROR: Unknown character: " + char);
                    break;
            }
        });
    });

    // Animate
    for (var step = 1; step <= NUM_STEPS; step++) {
        var current = JSON.parse(JSON.stringify(pixels));

        for (var y = 0; y < DIM; y++) {
            for (var x = 0; x < DIM; x++) {
                var neighbors = getNeighbors(x,y);

                // Count neighbors that are ON
                var numOn = 0;
                for (var n of neighbors) {
                    var [xn,yn] = n;
                    if (current[xn][yn]) { numOn += 1; }
                }
                
                // A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
                if (current[x][y] && (numOn != 2 && numOn != 3)) {
                    pixels[x][y] = false;
                }

                // A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
                if (!current[x][y] && numOn == 3) {
                    pixels[x][y] = true;
                }
            }
        }
    }

    // Part 1
    var finalOn = 0;
    for (var y = 0; y < DIM; y++) {
        for (var x = 0; x < DIM; x++) {
            if (pixels[x][y]) { finalOn += 1;}
        }
    }
    return finalOn;
}

function part2(filename) {

    // Init screen
    var pixels = new Array();
    for (var i = 0; i < DIM; i++) {
        pixels[i] = new Array(DIM).fill(false);
    }

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    // Initialize screen state
    input.map((row,y) => {
        var chars = row.split("");
        chars.map((char,x) => {
            switch (char) {
                case "#":
                    pixels[x][y] = true;
                    break;
                case ".":
                    pixels[x][y] = false;
                    break;
                default:
                    console.log("ERROR: Unknown character: " + char);
                    break;
            }
        });
    });

    // Initialize stuck pixels
    pixels[    0][    0] = true;
    pixels[    0][DIM-1] = true;
    pixels[DIM-1][    0] = true;
    pixels[DIM-1][DIM-1] = true;

    // Animate
    for (var step = 1; step <= NUM_STEPS; step++) {
        var current = JSON.parse(JSON.stringify(pixels));

        for (var y = 0; y < DIM; y++) {
            for (var x = 0; x < DIM; x++) {
                var neighbors = getNeighbors(x,y);

                /*if ([x,y] == [0,0] ||
                    [x,y] == [0,DIM-1] ||
                    [x,y] == [DIM-1,0] ||
                    [x,y] == [DIM-1,DIM-1]) { continue; }*/

                if (
                (x == 0 && y == 0) ||
                (x == 0 && y == DIM-1) ||
                (x == DIM-1 && y == 0) ||
                (x == DIM-1 && y == DIM-1)) { continue; }

                // Count neighbors that are ON
                var numOn = 0;
                for (var n of neighbors) {
                    var [xn,yn] = n;
                    if (current[xn][yn]) { numOn += 1; }
                }
                
                // A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
                if (current[x][y] && (numOn != 2 && numOn != 3)) {
                    pixels[x][y] = false;
                }

                // A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
                if (!current[x][y] && numOn == 3) {
                    pixels[x][y] = true;
                }
            }
        }
    }

    // Part 2
    var finalOn = 0;
    for (var y = 0; y < DIM; y++) {
        for (var x = 0; x < DIM; x++) {
            if (pixels[x][y]) { finalOn += 1;}
        }
    }
    return finalOn;
}

console.log(chalk.blue("Part 1: ") + chalk.yellow(part1(args[0]))); // 768
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2(args[0]))); // 781