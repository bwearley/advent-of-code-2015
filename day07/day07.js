'use strict';

const chalk = require('chalk');
const fs = require('fs');

var args = process.argv.slice(2);

function isNumeric(num){
    return !isNaN(num)
}

function Node(fromString) {
    this.string = fromString;

    var parts = fromString.split(" ");
    // 0 -> c
    if (parts[0].match("^[0-9]+$") && parts.length == 3) { // signal
        this.source = "Signal";
        this.destination = parts[2];
        this.value = Number(parts[0]);
    
    // lx -> a
    } else if ((parts[0].match("^[a-z]+$") && parts.length == 3)) {
        this.source = "Wire";
        this.destination = parts[2];
        this.value = parts[0];
    
    // NOT el -> em
    // fj LSHIFT 15 -> fn
    } else {
        this.source = "Gate";
        this.destination = parts[parts.length-1];
        if (parts[0] == "NOT") {
            this.cmd = parts[0];
            this.value = parts[1];
        } else {
            this.cmd = parts[1];
            this.values = [parts[0], parts[2]];
        }
    }
}

var wires = {};
var nodes = [];

function day07(filename) {

    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    input.map(line => nodes.push(new Node(line)));

    return getWireValue("a");

}

function getWireValue(wire) {
    if (wire in wires) {
        return wires[wire];
    } else {
        var sourceNode = nodes.find( ({destination}) => destination == wire);
        switch (sourceNode.source) {
            case "Signal":
                wires[sourceNode.destination] = sourceNode.value;
                return wires[sourceNode.destination];
                break;
            case "Gate":
                
                // Solve for required values
                if (sourceNode.value) {
                    if (sourceNode.value.match("^[a-z]+$")) {
                        var val0 = getWireValue(sourceNode.value);
                    } else {
                        var val0 = sourceNode.value;
                    }
                }   
                if (sourceNode.values) {
                    if (sourceNode.values[0].match("^[a-z]+$")) {
                        var val1 = getWireValue(sourceNode.values[0]);
                    } else {
                        var val1 = sourceNode.values[0];
                    }
                    if (sourceNode.values[1].match("^[a-z]+$")) {
                        var val2 = getWireValue(sourceNode.values[1]);
                    } else {
                        var val2 = sourceNode.values[1];
                    }
                }
                switch (sourceNode.cmd) {
                    case "NOT":
                        wires[sourceNode.destination] = Number(~Number(val0));
                        return wires[sourceNode.destination];
                        break;
                    case "OR":
                        wires[sourceNode.destination] = Number(Number(val1) | Number(val2));
                        return wires[sourceNode.destination];
                        break;
                    case "AND":
                        wires[sourceNode.destination] = Number(Number(val1) & Number(val2));
                        return wires[sourceNode.destination];
                        break;
                    case "RSHIFT":
                        wires[sourceNode.destination] = Number(Number(val1) >> Number(val2));
                        return wires[sourceNode.destination];
                        break;
                    case "LSHIFT":
                        wires[sourceNode.destination] = Number(Number(val1) << Number(val2));
                        return wires[sourceNode.destination];
                        break;
                    default:
                        console.log("ERROR: Uknown command: " + sourceNode.cmd);
                        break;
                }
                break;
            case "Wire":
                wires[sourceNode.destination] = getWireValue(sourceNode.value);
                return wires[sourceNode.destination];
                break;
            default:
                console.log("ERROR: Unknown source: " + sourceNode.source);
                return;
                break;
        }
    }
}

console.log("Part 1 Expected: " + 16076);
console.log("Part 2 Expected: " + 2797);
console.log(chalk.blue("Wire A Final Value: ") + chalk.yellow(day07(args[0])));