'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

const permutations = array => {
    if (array.length < 2) {
        // Base case, return single-element array wrapped in another array
        return [array];
    } else {
        let perms = [];
        for (let index = 0; index < array.length; index++) {
            // Make a fresh copy of the passed array and remove the current element from it
            let rest = array.slice();
            rest.splice(index, 1);
            
            // Call our function on that sub-array, storing the result: an array of arrays
            let ps = permutations(rest);
            
            // Add the current element to the beginning of each sub-array and add the new
            // permutation to the output array
            const current = [array[index]]
            for (let p of ps) {
                perms.push(current.concat(p));
            }
        }
        return perms;
    }
};

class PersonHappinessDirective {
    constructor(line) {
        let parts = line.split(" ");
        this.person = parts[0];
        this.other = parts[10].slice(0, -1); // strip trailing period
        this.amount = Number(parts[3]) * (parts[2] == "gain" ? +1 : -1);
    }
}

function day13(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    const people = input.map(line => line.split(" ")[0]).filter((v,i,a) => a.indexOf(v) === i); // filter unique
    const happiness = input.map(line => new PersonHappinessDirective(line));

    // Part 1
    var part1 = 0;
    var combos = permutations(people);
    for (var combo of combos) {
        var happy = 0;
        for (var p = 0; p < people.length; p++) {
            var left  = p == 0 ? people.length-1 : p-1;
            var right = p == people.length-1 ? 0 : p+1;
            happy += happiness.find(h => h.person == combo[p] && h.other == combo[left]).amount;
            happy += happiness.find(h => h.person == combo[p] && h.other == combo[right]).amount;
        }
        if (happy > part1) part1 = happy;
    }

    // Part 2
    var part2 = 0;
    people.push("Me");
    var combos = permutations(people);
    for (var combo of combos) {
        var happy = 0;
        for (var p = 0; p < people.length; p++) {
            if (combo[p] == "Me") continue;
            var left  = p == 0 ? people.length-1 : p-1;
            var right = p == people.length-1 ? 0 : p+1;
            happy += (combo[left]  != "Me") ? happiness.find(h => h.person == combo[p] && h.other == combo[left]).amount : 0;
            happy += (combo[right] != "Me") ? happiness.find(h => h.person == combo[p] && h.other == combo[right]).amount : 0;
        }
        if (happy > part2) part2 = happy;
    }

   return [part1,part2];
}

const [part1,part2] = day13(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 664
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 640