'use strict';

const chalk = require('chalk');
const fs = require('fs');

var args = process.argv.slice(2);

function part1(filename) {
    // Seems to be the best way to make a multidimensional array in JS
    var pixels = new Array();
    for (var i = 0; i < 1000; i++) {
        pixels[i] = new Array(1000).fill(false);
    }
    
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    input
        .map(s => {
            var parts = s.split(" ");
            switch (parts[0]) {
                case ("toggle"):
                    var start = parts[1].split(",").map(n => Number(n));
                    var end = parts[3].split(",").map(n => Number(n));
                    for (var x = start[0]; x <= end[0]; x++) {
                        for (var y = start[1]; y <= end[1]; y++) {
                            if (pixels[x][y]) {
                                pixels[x][y] = false;
                            } else {
                                pixels[x][y] = true;
                            }
                        }
                    }
                    break;
                case ("turn"):
                    var start = parts[2].split(",").map(n => Number(n));
                    var end = parts[4].split(",").map(n => Number(n));
                    switch (parts[1]) {
                        case ("off"):
                            for (var x = start[0]; x <= end[0]; x++) {
                                for (var y = start[1]; y <= end[1]; y++) {
                                    pixels[x][y] = false;
                                }
                            }
                            break;
                        case ("on"):
                            for (var x = start[0]; x <= end[0]; x++) {
                                for (var y = start[1]; y <= end[1]; y++) {
                                    pixels[x][y] = true;
                                }
                            }
                            break;
                        default:
                            console.log("Error: Unknown turn off/on type");
                            break;
                    }
                    break;
                default:
                    console.log("Error: Unknown command type.");
                    break;
            }
        }
    );
    
    var part1 = 0;
    for (var x = 0; x < 1000; x++) {
        for (var y = 0; y < 1000; y++) {
            if (pixels[x][y]) { part1 += 1; }
        }
    }

    return part1;
}

function part2(filename) {
    // Seems to be the best way to make a multidimensional array in JS
    var pixels = new Array();
    for (var i = 0; i < 1000; i++) {
        pixels[i] = new Array(1000).fill(0);
    }
    
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    input
        .map(s => {
            var parts = s.split(" ");
            switch (parts[0]) {
                case ("toggle"):
                    var start = parts[1].split(",").map(n => Number(n));
                    var end = parts[3].split(",").map(n => Number(n));
                    for (var x = start[0]; x <= end[0]; x++) {
                        for (var y = start[1]; y <= end[1]; y++) {
                            pixels[x][y] += 2;
                        }
                    }
                    break;
                case ("turn"):
                    var start = parts[2].split(",").map(n => Number(n));
                    var end = parts[4].split(",").map(n => Number(n));
                    switch (parts[1]) {
                        case ("off"):
                            for (var x = start[0]; x <= end[0]; x++) {
                                for (var y = start[1]; y <= end[1]; y++) {
                                    pixels[x][y] -= 1;
                                    pixels[x][y] = Math.max(0, pixels[x][y]);
                                }
                            }
                            break;
                        case ("on"):
                            for (var x = start[0]; x <= end[0]; x++) {
                                for (var y = start[1]; y <= end[1]; y++) {
                                    pixels[x][y] += 1;
                                }
                            }
                            break;
                        default:
                            console.log("Error: Unknown turn off/on type");
                            break;
                    }
                    break;
                default:
                    console.log("Error: Unknown command type.");
                    break;
            }
        }
    );
    
    var part2 = 0;
    for (var x = 0; x < 1000; x++) {
        for (var y = 0; y < 1000; y++) {
            part2 += pixels[x][y];
        }
    }

    return part2;
}

console.log(chalk.blue("Part 1: ") + chalk.yellow(part1(args[0]))); // 569999
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2(args[0]))); // 17836115