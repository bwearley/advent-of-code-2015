'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

const MAX_TEASPOONS = 100;

class Ingredient {
    constructor(line) {
        let name = line.match(/^(\w+)/g)[0];
        let numbers = line.match(/(\-?\d+)/g);
        this.name       = name;
        this.capacity   = Number(numbers[0]);
        this.durability = Number(numbers[1]);
        this.flavor     = Number(numbers[2]);
        this.texture    = Number(numbers[3]);
        this.calories   = Number(numbers[4]);
    }
}

function day15(filename) {

    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    const ingredients = input.map(line => new Ingredient(line));

    var part1 = 0;
    var part2 = 0;
    for (const combo of combinations()) {
        let score = scoreOfIngredientsCombination(combo,ingredients);
        let calories = caloriesOfIngredientsCombination(combo,ingredients);
        part1 = Math.max(score,part1);
        if (calories == 500) {
            part2 = Math.max(score, part2);
        }
    }
  
    return [part1, part2];
}

function* combinations() {
    for (var i1 = 0; i1 <= MAX_TEASPOONS; i1++) {
        for (var i2 = 0; i2 <= MAX_TEASPOONS - i1; i2++) {
            for (var i3 = 0; i3 <= MAX_TEASPOONS - i1 - i2; i3++) {
                yield [i1, i2, i3, MAX_TEASPOONS - i1 - i2 - i3];
            }
        }
    }
}

function scoreOfIngredientsCombination(combination,ingredients) {
    //    durability - ,     .- flavor
    //   capacity - ,   \  /   .- texture
    //               v  v  v  v
    var subscores = [0, 0, 0, 0];
    combination
        .map((amount,ix) => {
            subscores[0] += amount * ingredients[ix].capacity,
            subscores[1] += amount * ingredients[ix].durability,
            subscores[2] += amount * ingredients[ix].flavor,
            subscores[3] += amount * ingredients[ix].texture;
        });
    let score = subscores
        .map(sc => Math.max(sc,0))
        .reduce((a,b) => a*b);
    return score;
}

function caloriesOfIngredientsCombination(combination,ingredients) {
    let calories = combination
        .map((amount,ix) => amount * ingredients[ix].calories)
        .reduce((a,b) => a+b,0);
    return calories;
}

const [part1,part2] = day15(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 13882464
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 11171160