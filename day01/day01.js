'use strict';

const chalk = require('chalk');
const fs = require('fs');

var args = process.argv.slice(2);

function day01(filename) {
    var pos = 0;
    var ans = 0;
    const text = fs.readFileSync(filename)
        .toString()
        .split("")
        .map((ch,ix) => {
            if (ch == "(") {
                pos += 1;
            } else {
                pos -= 1;
            }
            if (pos == -1 && ans == 0) { ans = ix + 1; }
        });
    return [pos, ans];
}

const [part1,part2] = day01(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 74
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 1795