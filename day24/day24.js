'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

//var combine = function(a, min) {
var combine = function(a, min, max) {
    var fn = function(n, src, got, all) {
        if (n == 0) {
            if (got.length > 0) {
                all[all.length] = got;
            }
            return;
        }
        for (var j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
        }
        return;
    }
    var all = [];
    //for (var i = min; i < a.length; i++) {
    for (var i = min; i < max; i++) {
        fn(i, a, [], all);
    }
    all.push(a);
    return all;
}

function lowestQuantumEntanglementForGroups(gifts,numGroups) {
    const weightPerGroup = gifts.reduce((a,b) => a+b,0) / numGroups;
    let combos = combine(gifts,4,10)
        .filter(set => set.reduce((a,b) => a+b,0) == weightPerGroup); // limit group size to 10
    return calculcateQuantumEntanglement(combos[0]);
}

function calculcateQuantumEntanglement(gifts) {
    return gifts.reduce((a,b) => a*b,1);
}

function day24(filename) {

    // Load input
    const input = fs.readFileSync(filename)
      .toString()
      .split("\n")
      .filter(s => s.length > 0);
    const gifts = input.map(line => Number(line));
   
    const part1 = lowestQuantumEntanglementForGroups(gifts,3);
    const part2 = lowestQuantumEntanglementForGroups(gifts,4);

    return [part1,part2];
}

const [part1,part2] = day24(args[0]);
// Part 1: 113,109,107,101,89,1
// Part 2: 113,109,107,61
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 11846773891
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 80393059