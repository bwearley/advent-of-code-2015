'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

const permutations = array => {
    if (array.length < 2) {
        // Base case, return single-element array wrapped in another array
        return [array];
    } else {
        let perms = [];
        for (let index = 0; index < array.length; index++) {
            // Make a fresh copy of the passed array and remove the current element from it
            let rest = array.slice();
            rest.splice(index, 1);
            
            // Call our function on that sub-array, storing the result: an array of arrays
            let ps = permutations(rest);
            
            // Add the current element to the beginning of each sub-array and add the new
            // permutation to the output array
            const current = [array[index]]
            for (let p of ps) {
                perms.push(current.concat(p));
            }
        }
        return perms;
    }
};

function day09(filename) {

    var distances = [];
    var cities = [];
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .map(line => {
            // Snowdin to Tambi = 22
            let parts = line.split(" ");
            if (!cities.find(c => c == parts[0])) { cities.push(parts[0]); }
            if (!cities.find(c => c == parts[2])) { cities.push(parts[2]); }
            distances.push( [ parts[0], parts[2], Number(parts[4]) ] );
            distances.push( [ parts[2], parts[0], Number(parts[4]) ] );
        });

    var part1 = Number.MAX_SAFE_INTEGER;
    let combos = permutations(cities);
    for (var i = 0; i < combos.length; i++) {
        var dist = 0;
        for (var j = 0; j < cities.length-1; j++) {
            dist += distances.find(d => d[0] == combos[i][j] && d[1] == combos[i][j+1])[2];
        }
        if (dist < part1) { part1 = dist; }
    }

    var part2 = 0;
    for (var i = 0; i < combos.length; i++) {
        var dist = 0;
        for (var j = 0; j < cities.length-1; j++) {
            dist += distances.find(d => d[0] == combos[i][j] && d[1] == combos[i][j+1])[2];
        }
        if (dist > part2) { part2 = dist; }
    }
    return [part1, part2];
}

const [part1,part2] = day09(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 141
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 736