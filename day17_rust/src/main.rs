use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use itertools::Itertools;
extern crate itertools;

const EGGNOG_LITERS: u16 = 150;

fn day17(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let containers: Vec<u16> = input.iter().map(|x| x.parse::<u16>().unwrap()).collect();

    let combos = (3..containers.len())
        .flat_map(|x| containers.iter().combinations(x).collect::<Vec<Vec<_>>>())
        .collect::<Vec<Vec<_>>>();
    let combos = combos
        .iter()
        .filter(|x| x.iter().map(|&t| t).sum::<u16>() == EGGNOG_LITERS)
        .map(|x| x.iter().map(|&y| *y).collect::<Vec<u16>>())
        .collect::<Vec<Vec<u16>>>();

    let part1 = combos.len();
    let part2 = combos
        .iter()
        .filter(|y| y.len() == combos.iter().next().unwrap().len())
        .count();

    println!("Part 1: {}", part1); // 1638
    println!("Part 2: {}", part2); // 17

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day17(&filename).unwrap();
}