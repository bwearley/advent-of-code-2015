'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

var numberConstructor = Number(0).constructor;
//var stringConstructor = "test".constructor;
var arrayConstructor = [].constructor;
var objectConstructor = ({}).constructor;

function object_sum(obj) {
  var sum = 0;

  // Skip if any value == "red" for any key,value pair
  if (obj.constructor === objectConstructor) {
    for (const [key, value] of Object.entries(obj)) {
      if (value == "red") { return 0; }
    }
  }
 
  if (obj.constructor === objectConstructor) {
    for (const [key, value] of Object.entries(obj)) {
      sum += object_sum(value);
    }
  }

  if (obj.constructor === numberConstructor) {
      sum += Number(obj);
  }

  if (obj.constructor === arrayConstructor) {
    obj.map(v => sum += object_sum(v));
  }

  return sum;
}

function day12(filename) {

    const input = fs.readFileSync(filename).toString();

    var regexp = /(\-?\d+)/g;
    let numbers = input.match(regexp);
    let part1 = numbers.map(n => Number(n)).reduce((a,b) => a+b,0);

    let json = JSON.parse(input);
    let part2 = object_sum(json);

    return [part1, part2];
}

const [part1,part2] = day12(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 119433
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 68466