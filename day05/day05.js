'use strict';

const chalk = require('chalk');
const fs = require('fs');

var args = process.argv.slice(2);

function day05(filename) {
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    const part1 = input
        .filter(s => is_nice_part1(s))
        .length;

    const part2 = input
        .filter(s => is_nice_part2(s))
        .length;
    return [part1, part2];
}

function is_nice_part1(input) {
    var num_vowels = 0;
    var doubles = false;
    const vowels = "aeiou";

    if (input.includes("ab") || input.includes("cd") || input.includes("pq") || input.includes("xy")) { return false; }
    for (var i = 0; i < input.length; i++) {
        if (vowels.includes(input.charAt(i))) { num_vowels += 1; }
        if (i != input.length-1 && (input.charAt(i) == input.charAt(i+1))) { doubles = true; }
    }
    if (num_vowels >= 3 && doubles) { return true; }
}
function is_nice_part2(input) {
    var doubles = false;
    var repeats = false;
    // check for x_x pattern
    for (var i = 0; i < input.length-2; i++) {
        if (input.charAt(i) == input.charAt(i+2)) { doubles = true; }
    }
    // check for pair of 2 letters repeated twice
    for (var i = 0; i < input.length-1; i++) {
        for (var j = i + 2; j < input.length-1; j++) {
            if (input.charAt(i) == input.charAt(j) && input.charAt(i+1) == input.charAt(j+1)) {
                repeats = true;
                break;
            }
        }
        if (repeats) { break; }
    }
    if (doubles && repeats) { return true; }
}

const [part1,part2] = day05(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 236
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 51