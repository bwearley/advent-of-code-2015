'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function splitElements(str) {
    return str.match(/([A-Z][a-z]*)/g);
}

function day19(filename) {

    // Load input
    const input = fs.readFileSync(filename)
      .toString()
      .split("\n")
      .filter(s => s.length > 0);

    const replacements = input
        .slice(0,input.length-1)
        .map(line => line.split(" => "));
    var molecule = input[input.length-1];

    // Find each individual element in goal molecule
    let elements = splitElements(molecule);

    // Part 1
    let generated = {};
    elements.map((elem,eix) => {
        var matches = replacements.filter(rep => rep[0] == elem);
        matches.map((match,mix) => {
            var newMolecule = elements.slice(0,eix).join("") + match[1] + elements.slice(eix+1).join("");
            if (generated[newMolecule] == null) { generated[newMolecule] = true; }
        });
    });
    let part1 = Object.keys(generated).length;

    // Part 2
    molecule = molecule.split("").reverse().join(""); // no backtracking if processed backwards

    // Build dict of key => value pairs, then make master |-joined regex
    var repl2 = {};
    replacements
        .map(repl => {
            repl[0] = repl[0].split("").reverse().join("");
            repl[1] = repl[1].split("").reverse().join("");
            repl2[repl[1]] = repl[0];
        });
    let keysReg = new RegExp(Object.keys(repl2).join('|'));
    
    var part2 = 0;
    while (molecule != "e") {
        part2 += 1;
        molecule = molecule.replace(keysReg, repl2[molecule.match(keysReg)]);
    }

    return [part1,part2];
}

const [part1,part2] = day19(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 509
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 195