'use strict';

const chalk = require('chalk');
const fs = require('fs');
const HashMap = require('hashmap');

var args = process.argv.slice(2);

function part1(filename) {
    var part1 = 0;
    var [x,y] = [0, 0];
    var map = new HashMap;
    const text = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .join()
        .split("")
        .map(c => {
            switch (c) {
                case "^":
                    y += 1;
                    break;
                case "v":
                    y -= 1;
                    break;
                case "<":
                    x -= 1;
                    break;
                case ">":
                    x += 1;
                    break;
                default:
                    console.log("Error: Unknown character: " + c);
            }
            if (!map.has([x,y])) {
                map.set([x,y]);
                part1 += 1;
            }
        });
    return part1;
}

function part2(filename) {
    var part2 = 0;
    var [x1,y1] = [0, 0];
    var [x2,y2] = [0, 0];
    var map = new HashMap;
    const text = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .join()
        .split("")
        .map((c,ix) => {
            switch (c) {
                case "^":
                    if (ix % 2 == 0) { y1 += 1 } else { y2 += 1 }
                    break;
                case "v":
                    if (ix % 2 == 0) { y1 -= 1 } else { y2 -= 1 }
                    break;
                case "<":
                    if (ix % 2 == 0) { x1 -= 1 } else { x2 -= 1 }
                    break;
                case ">":
                    if (ix % 2 == 0) { x1 += 1 } else { x2 += 1 }
                    break;
                default:
                    console.log("Error: Unknown character: " + c);
            }
            if (ix % 2 == 0) {
                if (!map.has([x1,y1])) {
                    map.set([x1,y1]);
                    part2 += 1;
                }
            } else {
                if (!map.has([x2,y2])) {
                    map.set([x2,y2]);
                    part2 += 1;
                }
            }

        });
    return part2;
}

console.log(chalk.blue("Part 1: ") + chalk.yellow(part1(args[0]))); // 2572
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2(args[0]))); // 2631