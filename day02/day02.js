'use strict';

const chalk = require('chalk');
const fs = require('fs');

var args = process.argv.slice(2);

function day02(filename) {
    var total_area = 0;
    var ribbon_length = 0;
    const text = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .map(line => {
            let dims = line
                .split("x")
                .map(n => Number(n))
                .sort(function(a,b){return a-b});
            total_area += 3*dims[0]*dims[1] + 2*dims[1]*dims[2] + 2*dims[0]*dims[2];
            ribbon_length += 2*dims[0] + 2*dims[1] + dims[0]*dims[1]*dims[2];
        });
    return [total_area, ribbon_length];
}

const [part1,part2] = day02(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 1598415
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 3812909