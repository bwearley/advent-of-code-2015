'use strict';

const chalk = require('chalk');
const fs = require('fs');
const md5 = require('md5');

var args = process.argv.slice(2);

function part1(filename) {
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .join();

    var i = 0;
    while (true) {
        const hash = md5(input + i.toString());
        if (hash.startsWith("00000")) { return i; }
        i += 1;
    }
}

function part2(filename) {
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .join();

    var i = 0;
    while (true) {
        const hash = md5(input + i.toString());
        if (hash.startsWith("000000")) { return i; }
        i += 1;
    }
}

console.log(chalk.blue("Part 1: ") + chalk.yellow(part1(args[0]))); // 282749
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2(args[0]))); // 9962624