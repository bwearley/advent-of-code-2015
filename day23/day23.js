'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

class Instruction {
    constructor(string) {
        var parts = string.split(" ");
        this.instr = parts[0];
        if (parts.length == 2) {
            this.r = parts[1];
        } else {
            this.r = parts[1].match(/\w/g);
            this.offset = Number(parts[2]);
        }
    }
}

function regAddr(addr) {
    return String(addr).charCodeAt(0) - 97;
}

function part1(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);
    const instrs = input.map(line => new Instruction(line));

    // Registers
    var reg = new Array(2).fill(0);
    var cur = 0;
    while (true) {
        switch (instrs[cur].instr) {
            case "hlf":
                reg[regAddr(instrs[cur].r)] /= 2;
                cur += 1;
                break;
            case "tpl":
                reg[regAddr(instrs[cur].r)] *= 3;
                cur += 1;
                break;
            case "inc":
                reg[regAddr(instrs[cur].r)] += 1;
                cur += 1;
                break;
            case "jmp":
                cur += Number(instrs[cur].r);
                break;
            case "jie":
                if (reg[regAddr(instrs[cur].r)] % 2 == 0) {
                    cur += instrs[cur].offset;
                } else {
                    cur += 1;
                }
                break;
            case "jio":
                if (reg[regAddr(instrs[cur].r)] == 1) {
                    cur += instrs[cur].offset;
                } else {
                    cur += 1;
                }
                break;
            case "default":
                console.log("Unknown error");
                break;
        }
        if (cur >= instrs.length) { break; }
    }
    return reg[regAddr("b")];
}

function part2(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);
    const instrs = input.map(line => new Instruction(line));

    // Registers
    var reg = new Array(2).fill(0);
    var cur = 0;
    reg[0] = 1;
    while (true) {
        switch (instrs[cur].instr) {
            case "hlf":
                reg[regAddr(instrs[cur].r)] /= 2;
                cur += 1;
                break;
            case "tpl":
                reg[regAddr(instrs[cur].r)] *= 3;
                cur += 1;
                break;
            case "inc":
                reg[regAddr(instrs[cur].r)] += 1;
                cur += 1;
                break;
            case "jmp":
                cur += Number(instrs[cur].r);
                break;
            case "jie":
                if (reg[regAddr(instrs[cur].r)] % 2 == 0) {
                    cur += instrs[cur].offset;
                } else {
                    cur += 1;
                }
                break;
            case "jio":
                if (reg[regAddr(instrs[cur].r)] == 1) {
                    cur += instrs[cur].offset;
                } else {
                    cur += 1;
                }
                break;
            case "default":
                console.log("Unknown error");
                break;
        }
        if (cur >= instrs.length) { break; }
    }
    return reg[regAddr("b")];
}

console.log(chalk.blue("Part 1: ") + chalk.yellow(part1(args[0]))); // 255
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2(args[0]))); // 334