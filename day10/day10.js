'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function look_and_say(input) {
    var result = "";
    var current_c = " ";
    var current_count = 0;
    for (var i = 0; i < input.length; i++) {
        if (input.charAt(i) != current_c) {
            if (current_count > 0) { result += current_count + current_c; }
            current_c = input.charAt(i);
            current_count = 0;
        }
        current_count += 1;
    }
    if (current_count > 0) { result += current_count + current_c; }
    return result;
}

function day10(filename) {

    const input = fs.readFileSync(filename).toString();

    var part1 = input;
    for (var i = 0; i < 40; i++) {
        part1 = look_and_say(part1);
    }
    part1 = part1.length;

    let part2 = input;
    for (var i = 0; i < 50; i++) {
        part2 = look_and_say(part2);
    }
    part2 = part2.length;

    return [part1, part2];
}

const [part1,part2] = day10(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 252594
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 3579328