use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn look_and_say(input: &str) -> String {
    let mut result = String::new();
    let mut current_c: char = ' ';
    let mut current_count: u64 = 0;
    for c in input.chars() {
        if c != current_c {
            if current_count > 0 { result.push_str(&format!("{}{}", current_count, current_c)) }
            current_c = c;
            current_count = 0;
        }
        current_count += 1;
    }
    if current_count > 0 { result.push_str(&format!("{}{}", current_count, current_c)) }
    result
}

fn day10(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();

    // Part 1
    let mut part1 = input.clone();
    for _ in 0..40 {
        part1 = look_and_say(&part1);
    }
    println!("Part 1: {}", part1.len()); // 252594

    // Part 2
    let mut part2 = input.clone();
    for _ in 0..50 {
        part2 = look_and_say(&part2);
    }
    println!("Part 2: {}", part2.len()); // 3579328

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day10(&filename).unwrap();
}