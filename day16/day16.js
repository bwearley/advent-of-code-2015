'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

class ItemAmount {
    constructor(name,amount) {
        this.name = name;
        this.amount = amount;
    }
}

class Aunt {
    constructor(line) {
        this.sueNumber = Number(line.match(/^Sue (\d+):/)[1]);
        this.items = [...line.match(/(\w+: \d+)+/g)]
            .map(item => {
                let parts = item.split(": ");
                return new ItemAmount(parts[0],Number(parts[1]));
            });
        this.possible1 = true;
        this.possible2 = true;
    }
}

function day16(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    const aunts = input.map(line => new Aunt(line));

    const mfcsam = [
        new ItemAmount("children",3),
        new ItemAmount("cats",7),
        new ItemAmount("samoyeds",2),
        new ItemAmount("pomeranians",3),
        new ItemAmount("akitas",0),
        new ItemAmount("vizslas",0),
        new ItemAmount("goldfish",5),
        new ItemAmount("trees",3),
        new ItemAmount("cars",2),
        new ItemAmount("perfumes",1),
    ];

    // Part 1
    for (const aunt of aunts) {
        for (const item of aunt.items) {
            if (!mfcsam.find(it => it.name == item.name && it.amount == item.amount)) {
                aunt.possible1 = false;
            }
        }
    }
    let part1 = aunts.find(aunt => aunt.possible1).sueNumber;

    // Part 2
    for (const aunt of aunts) {
        for (const item of aunt.items) {
            if (item.name == "cats" || item.name == "trees") {
                if (!mfcsam.find(it => it.name == item.name && it.amount < item.amount)) {
                    aunt.possible2 = false;
                }
            } else if (item.name == "pomeranians" || item.name == "goldfish") {
                if (!mfcsam.find(it => it.name == item.name && it.amount > item.amount)) {
                    aunt.possible2 = false;
                }
            } else {
                if (!mfcsam.find(it => it.name == item.name && it.amount == item.amount)) {
                    aunt.possible2 = false;
                }
            }
        }
    }
    let part2 = aunts.find(aunt => aunt.possible2).sueNumber;

   return [part1,part2];
}

const [part1,part2] = day16(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 40
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 241