'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

const EGGNOG_LITERS = 150;

var combine = function(a, min) {
    var fn = function(n, src, got, all) {
        if (n == 0) {
            if (got.length > 0) {
                all[all.length] = got;
            }
            return;
        }
        for (var j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
        }
        return;
    }
    var all = [];
    for (var i = min; i < a.length; i++) {
        fn(i, a, [], all);
    }
    all.push(a);
    return all;
}

function day17(filename) {

    const containers = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .map(s => Number(s));

    let combos = combine(containers,2).filter(set => set.reduce((a,b) => a+b,0) == EGGNOG_LITERS);
    
    let part1 = combos.length;
    let part2 = combos.filter(set => set.length == combos[0].length).length;

    return [part1, part2];
}

const [part1,part2] = day17(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 1638
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 17