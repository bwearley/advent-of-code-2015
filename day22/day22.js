'use strict';

const fs = require('fs');
const chalk = require('chalk');
const dequeue = require('dequeue');
const clone = require('lodash.clone')
const clonedeep = require('lodash.clonedeep')

var args = process.argv.slice(2);

class Effect {
    constructor(name, mana, dmg, armor, duration) {
        this.name = name;
        this.mana = mana;
        this.dmg = dmg;
        this.armor = armor;
        this.duration = duration;
    }
}

//                       Spell    Rechrg  Dmg Armor  Dur
let shield = new Effect("Shield",      0,   0,    7,   6);
let poison = new Effect("Poison",      0,   3,    0,   6);
let rechrg = new Effect("Recharge",  101,   0,    0,   5);

class Spell {
    constructor(name, mana, dmg, heal, effect) {
        this.name = name;
        this.mana = mana;
        this.dmg = dmg;
        this.heal = heal;
        this.effect = effect;
    }
}

let spells = [
    //         Spell           Mana  Dmg  Heal   Effect 
    new Spell("Magic Missile",   53,   4,    0,    null),
    new Spell("Drain",           73,   2,    2,    null),
    new Spell("Shield",         113,   0,    0,  shield),
    new Spell("Poison",         173,   0,    0,  poison),
    new Spell("Recharge",       229,   0,    0,  rechrg),
];

class Player {
    constructor(hp, dmg, armor, mana) {
        this.hp = hp;
        this.dmg = dmg;
        this.armor = armor;
        this.mana = mana;
        this.effects = [];
    }
    possibleSpells() {
        // Possible spells are those for which there is enough mana, and those 
        // for which an effect is not currently active, UNLESS it is going to
        // end on this turn (hence the duration > 1 check)
        return spells
            .filter(s => 
                s.mana <= this.mana &&
                this.effects.filter(eff => eff.name == s.name && eff.duration > 1).length == 0);
    }
    castSpellOnEnemy(spell,enemy) {
        this.mana -= spell.mana;
        this.hp += spell.heal;
        enemy.hp -= spell.dmg;
        if (spell.effect != null) { this.effects.push(spell.effect); }
    }
    processEffectsWithEnemy(enemy) {
        for (const eff of this.effects) {
            this.mana += eff.mana;
            this.armor += eff.armor;
            enemy.hp -= eff.dmg;
            eff.duration -= 1;
        }
    }
    effectsCleanup() {
        this.armor = 0;
        this.effects = clonedeep(this.effects.filter(eff => eff.duration > 0));
    }
}

class GameState {
    constructor(player, boss, mana) {
        this.player = player;
        this.boss = boss;
        this.mana = mana;
    }
}

function day22(filename,hard=false) {

    // Load input
    const input = fs.readFileSync(filename).toString();
    const [boss_hp,boss_dmg] = input.match(/(\d+)/g).map(d => Number(d));

    const initialPlayer = new Player(50, 0, 0, 500);
    const initialBoss = new Player(boss_hp, boss_dmg, 0, 0);
    const initialState = new GameState(initialPlayer, initialBoss, 0);

    var minMana = Number.MAX_SAFE_INTEGER;

    var q = new dequeue();
    q.push(initialState);
    while (q.length > 0) {
        
        var state = q.pop();

        for (const sp of state.player.possibleSpells()) {
            var newMana = state.mana + sp.mana;
            if (newMana > minMana) { continue; }

            var player = clonedeep(state.player);
            var boss = clonedeep(state.boss);

            // Player's turn
            if (hard) { player.hp -= 1; }
            if (player.hp <= 0) { continue; }
            player.processEffectsWithEnemy(boss);
            player.castSpellOnEnemy(sp,boss);
            player.effectsCleanup();
            
            // Boss's turn
            player.processEffectsWithEnemy(boss);
            if (boss.hp <= 0) { minMana = Math.min(minMana, newMana); continue; }
            player.hp -= Math.max(boss.dmg - player.armor, 1);
            player.effectsCleanup();
            if (player.hp <= 0) { continue; }

            var newState = new GameState(player, boss, newMana);

            q.push(newState);
        }
    }
    return minMana;
}


console.log(chalk.blue("Part 1: ") + chalk.yellow(day22(args[0],false))); // 1824
console.log(chalk.blue("Part 2: ") + chalk.yellow(day22(args[0],true))); // 1937

function test() {

    const player = new Player(50, 0, 0, 500);
    const boss = new Player(71, 10, 0, 0);

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Shield";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Recharge";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Poison";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Shield";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Recharge";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Poison";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Shield";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Recharge";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Poison";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Shield";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Magic Missile";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Poison";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    console.log("Possibilities: " + player.possibleSpells().map(spl => spl.name));
    player.hp -= 1;
    player.processEffectsWithEnemy(boss);
    var newSpell = "Magic Missile";
    player.castSpellOnEnemy(spells.filter(sp => sp.name == newSpell)[0],boss);
    console.log("Player casts " + newSpell);
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();
    console.log("Boss HP: " + boss.hp);

}

/*function example1() {

    const player = new Player(10, 0, 0, 250);
    const boss = new Player(13, 8, 0, 0);
    //const initialState = new GameState(initialPlayer, initialBoss, 0);

    var minMana = Number.MAX_SAFE_INTEGER;

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + ", " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.castSpellOnEnemy(spells.filter(sp => sp.name == "Poison")[0],boss);
    console.log("Player casts poison");
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + ", " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + ", " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.castSpellOnEnemy(spells.filter(sp => sp.name == "Magic Missile")[0],boss);
    console.log("Player casts Magic Missile");
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + ", " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();
    console.log("Boss has " + boss.hp + " hit points--DEAD");

}*/

/*function example2() {

    const player = new Player(10, 0, 0, 250);
    const boss = new Player(14, 8, 0, 0);
    //const initialState = new GameState(initialPlayer, initialBoss, 0);

    var minMana = Number.MAX_SAFE_INTEGER;

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.castSpellOnEnemy(spells.filter(sp => sp.name == "Recharge")[0],boss);
    console.log("Player casts Recharge");
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.castSpellOnEnemy(spells.filter(sp => sp.name == "Shield")[0],boss);
    console.log("Player casts Shield");
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.castSpellOnEnemy(spells.filter(sp => sp.name == "Drain")[0],boss);
    console.log("Player casts Drain");
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.castSpellOnEnemy(spells.filter(sp => sp.name == "Poison")[0],boss);
    console.log("Player casts Poison");
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();

    // Player's turn
    console.log("-- Player turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.castSpellOnEnemy(spells.filter(sp => sp.name == "Magic Missile")[0],boss);
    console.log("Player casts Magic Missile");
    player.effectsCleanup();
    
    // Boss's turn
    console.log("-- Boss turn --");
    console.log("Player has " + player.hp + " hit points, " + player.armor + " armor, " + player.mana + " mana");
    console.log("Boss has " + boss.hp + " hit points");
    player.processEffectsWithEnemy(boss);
    player.hp -= Math.max(boss.dmg - player.armor, 1);
    player.effectsCleanup();
    console.log("Boss has " + boss.hp + " hit points--DEAD");

}*/
