'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function day20(filename) {

    // Load input
    const input = fs.readFileSync(filename).toString();
    const numGifts = Number(input);

    const NUM_HOUSES = 1000000;

    // Part 1
    var houses = new Array(NUM_HOUSES).fill(0);
    for (var elf = 1; elf < NUM_HOUSES; elf++) {
        for (var h = elf; h < NUM_HOUSES; h += elf) {
            houses[h] += 10 * elf;
        }
    }
    let part1 = houses.findIndex(h => h >= numGifts);

    // Part 2
    var houses = new Array(NUM_HOUSES).fill(0);
    for (var elf = 1; elf < NUM_HOUSES; elf++) {
        for (var h = elf; h <= elf * 50; h += elf) {
            houses[h] += 11 * elf;
        }
    }
    let part2 = houses.findIndex(h => h >= numGifts);

    return [part1,part2];
}

const [part1,part2] = day20(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 831600
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 884520

/*
// Naive solution is actually much faster
/*
const factors = number => Array
    .from(Array(number + 1), (_, i) => i)
    .filter(i => number % i === 0);
var part1 = 0;
for (var i = 811000; ; i++) {
    console.log(i);
    if (factors(i).reduce((a,b) => a+b,0)*10 >= numGifts) { part1 = i; break; }
    //console.log(i + " " + factors(i).reduce((a,b) => a+b,0)*10);
}*/