'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function next(previous) {
    const mult = 252533;
    const div = 33554393;
    return (previous * mult) % div;
}

function day25(filename) {

    const start = 20151125;

    // Load input
    const input = fs.readFileSync(filename).toString();

    let row = Number(input.match(/row (\d+)/)[1]);
    let col = Number(input.match(/column (\d+)/)[1]);

    // First column: y = 0.5x^2 - 0.5x + 1
    // First row: y = 0.5x^2 + 0.5x
    const col1Intercept = row + col - 1;
    const col1InterceptValue = 0.5*Math.pow(col1Intercept,2) - 0.5*col1Intercept + 1;
    const numIters = col1InterceptValue + col - 1;

    var code = start;
    for (var i = 1; i < numIters; i++) {
        code = next(code);
    }
    return code;
}

console.log(chalk.blue("Day 25: ") + chalk.yellow(day25(args[0]))); // 9132360

/*
 +----------------------------------->
 |
 |
 |               *  <- target row, col
 |              /
 |             /
 |            /
 |           /
 |          /
 |         /
 |        /
 |       /
 |      /
 |     /
 |    /
 |   /
 |  /
 | /
 *       <- col1Intercept = row + col - 1
 |          value @ col1Intercept = 0.5x^2 - 0.5x + 1
 v
*/