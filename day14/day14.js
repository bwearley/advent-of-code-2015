'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function day14(filename) {

  const input = fs.readFileSync(filename)
    .toString()
    .split("\n")
    .filter(s => s.length > 0);

    const reindeer = input
        .map(l => {
            let parts = l.split(" ");
            // Dancer can fly 27 km/s for 5 seconds, but then must rest for 132 seconds.
            return [ parts[0], Number(parts[3]), Number(parts[6]), Number(parts[13])];
        });
    
    const part1 = Math.max(...distance_at_time(reindeer, 2503));
    const part2 = Math.max(...score_at_time(reindeer, 2503));
    return [part1, part2];
}

function distance_at_time(reindeer, time) {
    return reindeer
        .map(deer => {
        let speed = deer[1];
        let travel_time = deer[2];
        let rest_time = deer[3];
        let d = Math.floor(time / (travel_time + rest_time));
        let r = time % (travel_time + rest_time);
        return speed * (d * travel_time + Math.min(r, travel_time));
    });
}

function score_at_time(reindeer, time) {
    reindeer.map(r => r.push(0)); // add a score to each reindeer
    for (var t = 1; t < time; t++) {
        const dists = distance_at_time(reindeer, t);
        const max_dist = Math.max(...dists);
        dists.map((d,ix) => { if (d == max_dist) { reindeer[ix][4] += 1; }});    
    }
    return reindeer.map(r => {return r[4]});
}

const [part1,part2] = day14(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 2640
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 1102