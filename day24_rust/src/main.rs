use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::cmp::Ordering;

extern crate itertools;
use itertools::Itertools;

fn lowest_qe_for_groups(gifts: Vec<u64>, num_groups: u64) -> u64 {
    let target_group_weight: u64 = gifts.iter().sum::<u64>() / num_groups;
    let mut combinations: Vec<Vec<u64>> = Vec::new();
    for min_num in 1..gifts.len() {
        let combos: Vec<Vec<u64>> = gifts
            .clone()
            .iter()
            .combinations(min_num)
            .map(|x| x.iter().map(|y| **y).collect::<Vec<u64>>())
            .filter(|x| x.iter().sum::<u64>() == target_group_weight)
            .collect::<Vec<Vec<u64>>>();
        if combos.len() > 0 {
            combinations = combos.clone();
            break;
        }
    }

    // Sort and break ties
    combinations
        .sort_by(|a, b|
            b.iter().product::<u64>().cmp(&a.iter().product::<u64>()).reverse());

    calculcate_qe(combinations.iter().nth(0).unwrap())
}

fn calculcate_qe(gifts: &Vec<u64>) -> u64 {
    gifts.iter().product()
}

fn day24(input: &str) -> io::Result<()> {

    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);
    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let gifts: Vec<u64> = input.iter().map(|x| x.parse::<u64>().unwrap()).collect();

    let part1 = lowest_qe_for_groups(gifts.clone(), 3);
    println!("Part 1: {}", part1); // 11846773891

    let part2 = lowest_qe_for_groups(gifts.clone(), 4);
    println!("Part 2: {}", part2); // 80393059

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day24(&filename).unwrap();
}