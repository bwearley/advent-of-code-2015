'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

const BASE_PLAYER_HP = 100;

class Item {
    constructor(name, cost, dmg, armor) {
        this.name = name;
        this.cost = cost;
        this.dmg = dmg;
        this.armor = armor;
    }
}

let weapons = [
    //       Weapons        Cost  Damage Armor
    new Item("Dagger",        8,      4,    0),
    new Item("Shortsword",   10,      5,    0),
    new Item("Warhammer",    25,      6,    0),
    new Item("Longsword",    40,      7,    0),
    new Item("Greataxe",     74,      8,    0),
];

let armory = [
    //       Armor         Cost  Damage Armor
    new Item("None",          0,      0,    0),
    new Item("Leather",      13,      0,    1),
    new Item("Chainmail",    31,      0,    2),
    new Item("Splintmail",   53,      0,    3),
    new Item("Bandedmail",   75,      0,    4),
    new Item("Platemail",   102,      0,    5),
];

let rings = [
    //       Rings         Cost  Damage Armor
    new Item("None",          0,      0,    0),
    new Item("Damage +1",    25,      1,    0),
    new Item("Damage +2",    50,      2,    0),
    new Item("Damage +3",   100,      3,    0),
    new Item("Defense +1",   20,      0,    1),
    new Item("Defense +2",   40,      0,    2),
    new Item("Defense +3",   80,      0,    3),
];

class Player {
    constructor(hp, dmg, armor) {
        this.hp = hp;
        this.dmg = dmg;
        this.armor = armor;
    }
}

function battle(player, boss) {
    while (true) {
        // Player's turn
        boss.hp -= Math.max(player.dmg - boss.armor, 1);
        if (boss.hp <= 0) return true;
        
        // Boss's turn
        player.hp -= Math.max(boss.dmg - player.armor, 1);
        if (player.hp <= 0) return false;
    }
}

function* getInventory() {
    for (const weapon of weapons) {
        for (const armor of armory) {
            for (const ring1 of rings) {
                for (const ring2 of rings) {
                    if (ring1 == ring2) { continue; } // Shop only has 1 of each
                    yield [weapon, armor, ring1, ring2];
                }
            }
        }
    }
}

function day21(filename) {

    // Load input
    const input = fs.readFileSync(filename).toString().match(/\d+/g);
    let boss_hp = Number(input[0]);
    let boss_dmg = Number(input[1]);
    let boss_armor = Number(input[2]);

    // Simulate
    let min_cost = Number.MAX_SAFE_INTEGER;
    let max_cost = 0;
    for (const inventory of getInventory()) {
        let cost = inventory.map(item => item.cost).reduce((a,b) => a+b,0);
        let player = new Player(
            BASE_PLAYER_HP,
            inventory.map(item =>   item.dmg).reduce((a,b) => a+b,0),
            inventory.map(item => item.armor).reduce((a,b) => a+b,0)
        );
        let boss = new Player(boss_hp, boss_dmg, boss_armor);
        if (battle(player,boss)) {
            min_cost = Math.min(min_cost,cost);
        } else {
            max_cost = Math.max(max_cost,cost);
        }
    }
    return [min_cost,max_cost];
}

const [part1,part2] = day21(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 91
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 158