'use strict';

const fs = require('fs');
const chalk = require('chalk');
const base26 = require('base26');

var args = process.argv.slice(2);

function next_password(current_pw) {
    var pw = base26.add(current_pw, 0);
    for (var i = 1; ; i++) {
        pw = base26.add(current_pw, i);

        // Check for abc, def, etc.
        var has_repeat = false;
        for (var j = 0; j < pw.length-3; j++) {
            if (pw[j+1] == base26.add(pw[j],1) &&
                pw[j+2] == base26.add(pw[j],2)) {
                    has_repeat = true;
                    break;
                }
        }
        if (!has_repeat) { continue; }

        // Skip forbidden characters
        for (var ch in pw) {
            if (ch == "i" || ch == "o" || ch == "l") {
                continue;
            }
        }

        // Check for existence of non-overlapping pairs
        var num_pairs = 0;
        var skip = -1; // for avoid non-overlapping
        for (var j = 0; j < pw.length-1; j++) {
            if (j == skip) { continue; }
            if (pw[j] == pw[j+1]) {
                num_pairs += 1;
                skip = j + 1;
            }
        }
        if (num_pairs >= 2) { return pw; }
    }
}

function day11(filename) {

    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(l => l.length > 0)
        .toString();

    let part1 = next_password(input);
    let part2 = next_password(part1);

    return [part1, part2];
}

const [part1,part2] = day11(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // hxbxxyzz
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // hxcaabcc