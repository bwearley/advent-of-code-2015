'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function day08(filename) {

    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    let part1 = input
        .map(l => l.length - eval(l).length)
        .reduce((a,b) => a+b, 0);

    let part2 = input
        .map(l => JSON.stringify(l).length - l.length)
        .reduce((a,b) => a+b, 0);

    return [part1, part2];
}

const [part1,part2] = day08(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 1371
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 2117